const currencyEl_one = document.getElementById("currency-one");
const amountEl_one = document.getElementById("amount-one");
const currencyEl_two = document.getElementById("currency-two");
const amountEl_two = document.getElementById("amount-two");

const rateEl = document.getElementById("rate");
const swap = document.getElementById("swap");

/*
Cuando se realiza la consulta al API, indicar estado de espera mediante logo o mensaje por pantalla.
Hecho --> Si se produce un error al hacer la consulta al API mostrar un mensaje con el error.
Hecho --> No permitir números negativos.
*/

//Fetch exchange rate
function calculate() {
  const currency_one = currencyEl_one.value;
  const currency_two = currencyEl_two.value;

  const re = /^\d{1,}$/;

  if (re.test(amountEl_one.value)) {
    console.log("Numero positivo");

    fetch(`https://api.exchangerate-api.com/v4/latest/${currency_one}`)
      .then((res) => {
        console.log(res.status);
        return res.json();
      })
      .then((data) => {
        const rate = data.rates[currency_two];
        rateEl.innerText = `1 ${currency_one} = ${rate} ${currency_two}`;
        amountEl_two.value = (amountEl_one.value * rate).toFixed(2);
      })
      .catch(function (err) {
        //Si se produce un error al hacer la consulta al API mostrar un mensaje con el error.
        console.log(err.status);
      });
  } else {
    //No permitir números negativos.
    console.log("No negativos");
    amountEl_two.value = 0;
  }
}
currencyEl_one.addEventListener("change", calculate);
amountEl_one.addEventListener("input", calculate);
currencyEl_two.addEventListener("change", calculate);
amountEl_two.addEventListener("input", calculate);

swap.addEventListener("click", () => {
  const temp = currencyEl_one.value;
  currencyEl_one.value = currencyEl_two.value;
  currencyEl_two.value = temp;
  calculate();
});

calculate();
